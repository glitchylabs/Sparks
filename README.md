# Sparks
## Completely modularized software framework

# Pre-reqs
- Install [Node.js](https://nodejs.org/en/)
- Install [Yarn](https://yarnpkg.com/lang/en/)

# Getting started
- Clone the repository
```
git clone --depth=1 https://gitlab.com/glitchylabs/Sparks.git Sparks
```
- Install dependencies
```
cd Sparks
yarn install
```
- Build and run the project
```
yarn start
```

# TypeScript + Node 
Sparks was created to fill the void of a lot of the discord bots out there...Reliability and easy expandability.

## Project Structure

## Building the project