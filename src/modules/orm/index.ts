import * as DiskAdapter from "sails-disk";
import * as Waterline from "waterline";
import { EventBus } from "../eventBus";

export { Waterline };
export class ORM {

    private orm;
    private config;
    private running: boolean;

    constructor() {
        this.config = {

            adapters: {
                "sails-disk": DiskAdapter
            },

            datastores: {
                default: {
                    adapter: "sails-disk"
                }
            },

            models: {}
        };
    }

    public registerModel(model) {
        this.config.models[Object.keys(model)[0]] = model[Object.keys(model)[0]];
        if (this.running) {
            this.restart();
        }
    }

    public start() {
        return new Promise((resolve) => {
            Waterline.start(this.config, (err, orm) => {
                if (err) {
                    console.error("Could not start up the ORM:\n", err);
                    return process.exit(1);
                }
                this.running = true;
                this.orm = orm;

                EventBus.emit("message", "ORM loaded");
                resolve();
            });
        });
    }

    private stop() {
        return new Promise((resolve) => {
            Waterline.stop(this.orm, (err) => {
                if (err) {
                    console.error("Could not start up the ORM:\n", err);
                    return process.exit(1);
                }
                this.running = false;
                resolve();
            });
        });
    }

    private restart() {
        this.stop().then(this.start.bind(this));
    }

    // Get Model for data
    public getModel(table) {
        return this.orm.collections[table];
    }
}
