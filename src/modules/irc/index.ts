import { Core } from "..";
import { EventBus } from "../eventBus";
import { Client, ClientMode, Message } from "irc";

export class Irc {
    private core = Core.getInstance();
    private client: Client;
    private options: {
        mode?: ClientMode;
        server?: string;
        port?: string;
        nickname?: string;
    };
    private commands = {
        help: {
            method: this.help.bind(this),
            description: "Calls this help screen..."
        }
    };
    private messagePrefix: string;

    constructor() {
        EventBus.on("register", this.register.bind(this));
        EventBus.on("message", this.handleMessage.bind(this));

        EventBus.emit("register", {
            name: "Irc",
            target: "Core",
            commands: {
            }
        });

        this.options = {};
    }

    /**
     * Handle messages from a module.
     */
    handleMessage(message) {
        if (message === "ORM loaded") {
            /*
            * This is needed to set the data for the first time...I'll probably make a "Setup" module later.
            * this.core.setConfig("irc:clientToken", "1234567890"); - Get this token from Discord Developers page.
            * this.core.setConfig("irc:prefix", "!");  
            */
            this.core.getConfig("irc:prefix").then((prefix) => {
                this.messagePrefix = prefix;
            });
            this.core.setConfig("irc:mode", "tls");
            this.core.setConfig("irc:server", "irc.tas.bot");
            this.core.setConfig("irc:port", "6667");
            this.core.setConfig("irc:nickname", "Test-Bot");
            // this.core.getConfig("irc:").then(() => {  
            // });
            const promises = [
                this.core.getConfig("irc:mode").then((x) => this.options.mode = x),
                this.core.getConfig("irc:server").then((x) => this.options.server = x),
                this.core.getConfig("irc:port").then((x) => this.options.port = x),
                this.core.getConfig("irc:nickname").then((x) => this.options.nickname = x)
            ];

            Promise.all(promises).then(() => {
                this.client = new Client(this.options.mode, this.options.server, this.options.port, this.options.nickname);
                this.client.connect();
                this.client.joinChannel("#bot");
                this.client.on("message", (msg: Message) => {
                    switch (msg.command.toUpperCase()) {
                        case "422":
                        case "376":
                            console.log(`Logged in as ${this.client.nickname}!`);
                            break;
                        case "PRIVMSG":
                            this.ircMessage(msg);
                            break;
                        default:
                            console.log(`Some message i don't know about: ${msg.command}, ${msg.raw}`);
                            break;
                    }

                });
            });

            EventBus.emit("register", {
                name: "Irc",
                target: "Echo",
            });
        }
    }

    ircMessage(msg: Message) {
        const channel = (msg.channel.startsWith("#")) ? msg.channel : msg.sender;
        if (msg.message.startsWith(this.messagePrefix)) {
            const message = msg.message.replace(this.messagePrefix, "");
            if (this.commands.hasOwnProperty(message.split(" ")[0])) {
                this.commands[message.split(" ")[0]].method(message).then((response) => {
                    switch (response.type) {
                        case "table":
                            const headers = [];
                            const lines = [];
                            const maxlens = new Array(response.content.headers.length).fill(0);
                            response.content.headers.forEach((header, index) => {
                                if (header.length > maxlens[index]) maxlens[index] = header.length;
                            });
                            response.content.rows.forEach((row) => {
                                row.forEach((value, index) => {
                                    if (value.length > maxlens[index]) maxlens[index] = value.length;
                                });
                            });
                            response.content.headers.forEach((header, index) => {
                                headers.push(`\x02${header.padEnd(maxlens[index])}\x02`);
                            });
                            lines.push(headers.join(" | "));
                            response.content.rows.forEach((row) => {
                                const line = [];
                                row.forEach((value, index) => {
                                    line.push(`${value}`.padEnd(maxlens[index]));
                                });
                                lines.push(line.join(" | "));
                            });
                            this.client.sendData(`PRIVMSG ${channel} :${response.content.title}`, false);
                            lines.forEach((line) => {
                                this.client.sendData(`PRIVMSG ${channel} :${line}`, false);
                            });
                            break;
                        default:
                            this.client.sendData(`PRIVMSG ${channel} :${response.content}`, false);
                            break;
                    }
                });
            }
        }
    }

    register(module) {
        if (module.target == "Irc") {
            this.commands = { ...this.commands, ...module.commands };
        }
    }

    help() {
        const helpfile = {
            type: "table",
            content: {
                title: "Let's see how i can help:",
                headers: ["Command", "Description"],
                rows: []
            }
        };
        Object.keys(this.commands).forEach((command) => {
            helpfile.content.rows.push([command, this.commands[command].description]);
        });

        return new Promise((resolve) => {
            resolve(helpfile);
        });
    }
}